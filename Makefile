MAKEFLAGS += --warn-undefined-variables
.SHELLFLAGS := -eu -o pipefail -c

all: help
.PHONY: all

# Use bash for inline if-statements
SHELL:=bash

##@ Helpers
help: ## display this help
	@echo "docker-tagging"
	@echo "====================="
	@echo "Replace % with a valid semver string with patch, minor, major, prepatch,"
	@echo "preminor, premajor or prerelease (e.g. make bumpversion/minor)."
	@awk 'BEGIN {FS = ":.*##"; printf "\033[36m\033[0m"} /^[a-zA-Z0-9_%/-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	@printf "\n"

##@ Versioning
bumpversion/%: ## bumps the version of the project and writes the new version back to pyproject.toml
ifeq ($(git status --porcelain=v1 2>/dev/null | wc -l),)
	@$(eval VERSION := $(shell poetry version -s))
	@$(eval NEW_VERSION := $(shell poetry version $(notdir $@)))
	@git commit -m "$(NEW_VERSION)" pyproject.toml --quiet --no-verify
	@git tag -a $(shell poetry version -s) -m "$(NEW_VERSION)"
	@echo $(NEW_VERSION)
else
	@echo "needs update: $(shell git update-index --really-refresh | cut -d':' -f1 )"
	$(info [WARNING] Unstaged files detected.)
endif

##@ Building and publishing
build: ## builds a package, as a tarball and a wheel by default
	@poetry build

publish: ## publishes a package to a remote repository
	@git push origin --tags
	@poetry publish --build --username ${PYPI_USERNAME} --password ${PYPI_PASSWORD}

##@ Development
dev-env: ## install libraries required to build docs and run tests
	@poetry install
