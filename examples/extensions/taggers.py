# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
from docker_tagging import (
    TaggerInterface,
    get_env_variable,
    get_pip_package_version,
    get_program_version
)


class PythonVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "python-" + get_program_version(container, "python").split()[1]


class JupyterNotebookVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "notebook-" + get_program_version(container, "jupyter-notebook")


class JupyterLabVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "lab-" + get_program_version(container, "jupyter-lab")


class JupyterHubVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "hub-" + get_program_version(container, "jupyterhub")


class RVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "r-" + get_program_version(container, "R").split()[2]


class TensorflowVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "tensorflow-" + get_pip_package_version(container, "tensorflow")


class JuliaVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "julia-" + get_program_version(container, "julia").split()[2]


class SparkVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "spark-" + get_env_variable(container, "APACHE_SPARK_VERSION")


class HadoopVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "hadoop-" + get_env_variable(container, "HADOOP_VERSION")


class JavaVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "java-" + get_program_version(container, "java").split()[1]


class PHPVersionTagger(TaggerInterface):
    @staticmethod
    def tag_value(container) -> str:
        return "php-" + get_program_version(container, "php").split()[1]
