# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
from docker_tagging import (
    ManifestInterface,
    quoted_output
)


class RPackagesManifest(ManifestInterface):
    @staticmethod
    def markdown_piece(container) -> str:
        return "\n".join(
            [
                "## R Packages",
                "",
                quoted_output(container, "R --version"),
                "",
                quoted_output(
                    container,
                    "R --silent -e 'installed.packages(.Library)[, c(1,3)]'",
                ),
            ]
        )


class JuliaPackagesManifest(ManifestInterface):
    @staticmethod
    def markdown_piece(container) -> str:
        return "\n".join(
            [
                "## Julia Packages",
                "",
                quoted_output(
                    container,
                    "julia -E 'using InteractiveUtils; versioninfo()'",
                ),
                "",
                quoted_output(container, "julia -E 'import Pkg; Pkg.status()'"),
            ]
        )


class SparkInfoManifest(ManifestInterface):
    @staticmethod
    def markdown_piece(container) -> str:
        return "\n".join(
            [
                "## Apache Spark",
                "",
                quoted_output(container, "/usr/local/spark/bin/spark-submit --version"),
            ]
        )
